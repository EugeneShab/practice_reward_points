<?php

/**
 * Reward action for new customer registration
 *
 * @category    Practice
 * @package     Practice_Reward
 */
class Practice_Reward_Model_Action_Register extends Practice_Reward_Model_Action_Abstract
{
    /**
     * Retrieve points delta for action
     *
     * @param int $websiteId
     * @return int
     */
    public function getPoints($websiteId)
    {
        return (int)Mage::helper('practice_reward')->getPointsConfig('register', $websiteId);
    }

    /**
     * Return action message for history log
     *
     * @param array $args Additional history data
     * @return string
     */
    public function getHistoryMessage($args = array())
    {
        return Mage::helper('practice_reward')->__('Registered as customer.');
    }
}
