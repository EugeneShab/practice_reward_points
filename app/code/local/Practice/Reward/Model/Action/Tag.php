<?php

/**
 * Reward action for tag submission
 *
 * @category    Practice
 * @package     Practice_Reward
 */
class Practice_Reward_Model_Action_Tag extends Practice_Reward_Model_Action_Abstract
{
    /**
     * Retrieve points delta for action
     *
     * @param int $websiteId
     * @return int
     */
    public function getPoints($websiteId)
    {
        return (int)Mage::helper('practice_reward')->getPointsConfig('tag', $websiteId);
    }

    /**
     * Return pre-configured limit of rewards for action
     *
     * @return int|string
     */
    public function getRewardLimit()
    {
        return Mage::helper('practice_reward')->getPointsConfig('tag_limit', $this->getReward()->getWebsiteId());
    }

    /**
     * Return action message for history log
     *
     * @param array $args Additional history data
     * @return string
     */
    public function getHistoryMessage($args = array())
    {
        $tag = isset($args['tag']) ? $args['tag'] : '';
        return Mage::helper('practice_reward')->__('For submitting tag (%s).', $tag);
    }

    /**
     * Setter for $_entity and add some extra data to history
     *
     * @param Varien_Object $entity
     * @return Practice_Reward_Model_Action_Abstract
     */
    public function setEntity($entity)
    {
        parent::setEntity($entity);
        $this->getHistory()->addAdditionalData(array(
            'tag' => $this->getEntity()->getName()
        ));
        return $this;
    }
}
