<?php

/**
 * Reward action for review submission
 *
 * @category    Practice
 * @package     Practice_Reward
 */
class Practice_Reward_Model_Action_Review extends Practice_Reward_Model_Action_Abstract
{
    /**
     * Retrieve points delta for action
     *
     * @param int $websiteId
     * @return int
     */
    public function getPoints($websiteId)
    {
        return (int)Mage::helper('practice_reward')->getPointsConfig('review', $websiteId);
    }

    /**
     * Return pre-configured limit of rewards for action
     *
     * @return int|string
     */
    public function getRewardLimit()
    {
        return Mage::helper('practice_reward')->getPointsConfig('review_limit', $this->getReward()->getWebsiteId());
    }

    /**
     * Return action message for history log
     *
     * @param array $args Additional history data
     * @return string
     */
    public function getHistoryMessage($args = array())
    {
        return Mage::helper('practice_reward')->__('For submitting a product review.');
    }
}
