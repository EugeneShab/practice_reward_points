<?php

/**
 * Source model for Acquiring frequency when Order processed after Invitation
 */
class Practice_Reward_Model_Source_Points_InvitationOrder
{
    public function toOptionArray()
    {
        return array(
            array('value' => '*', 'label' => Mage::helper('practice_reward')->__('Each')),
            array('value' => '1', 'label' => Mage::helper('practice_reward')->__('First')),
        );
    }
}
