<?php

/**
 * Source model for list of Expiry Calculation algorythms
 */
class Practice_Reward_Model_Source_Points_ExpiryCalculation
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'static', 'label' => Mage::helper('practice_reward')->__('Static')),
            array('value' => 'dynamic', 'label' => Mage::helper('practice_reward')->__('Dynamic')),
        );
    }
}
