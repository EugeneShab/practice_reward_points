<?php

/**
 * Reward rate collection
 *
 * @category    Practice
 * @package     Practice_Reward
 */
class Practice_Reward_Model_Resource_Reward_Rate_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Internal constructor
     *
     */
    protected function _construct()
    {
        $this->_init('practice_reward/reward_rate');
    }

    /**
     * Add filter by website id
     *
     * @param integer|array $websiteId
     * @return Practice_Reward_Model_Resource_Reward_Rate_Collection
     */
    public function addWebsiteFilter($websiteId)
    {
        $websiteId = array_merge((array)$websiteId, array(0));
        $this->getSelect()->where('main_table.website_id IN (?)', $websiteId);
        return $this;
    }
}
