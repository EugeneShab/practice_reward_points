<?php

/**
 * Reward collection
 *
 * @category    Practice
 * @package     Practice_Reward
 */
class Practice_Reward_Model_Resource_Reward_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Internal construcotr
     *
     */
    protected function _construct()
    {
        $this->_init('practice_reward/reward');
    }

    /**
     * Add filter by website id
     *
     * @param integer|array $websiteId
     * @return Practice_Reward_Model_Resource_Reward_Collection
     */
    public function addWebsiteFilter($websiteId)
    {
        $this->getSelect()
            ->where(is_array($websiteId) ? 'main_table.website_id IN (?)' : 'main_table.website_id = ?', $websiteId);
        return $this;
    }
}
