<?php

/**
 * Reward rate grid container
 *
 * @category    Practice
 * @package     Practice_Reward
 */
class Practice_Reward_Block_Adminhtml_Reward_Rate extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Block constructor
     */
    public function __construct()
    {
        $this->_blockGroup = 'practice_reward';
        $this->_controller = 'adminhtml_reward_rate';
        $this->_headerText = Mage::helper('practice_reward')->__('Manage Reward Exchange Rates');
        parent::__construct();
        $this->_updateButton('add', 'label', Mage::helper('practice_reward')->__('Add New Rate'));
    }
}
