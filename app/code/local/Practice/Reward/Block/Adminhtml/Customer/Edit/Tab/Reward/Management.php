<?php

/**
 * Reward management container
 *
 * @category    Practice
 * @package     Practice_Reward
 *
 */
class Practice_Reward_Block_Adminhtml_Customer_Edit_Tab_Reward_Management
    extends Mage_Adminhtml_Block_Template
{
    /**
     * Internal constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('practice/reward/customer/edit/management.phtml');
    }

    /**
     * Prepare layout
     *
     * @return Practice_Reward_Block_Adminhtml_Customer_Edit_Tab_Reward_Management
     */
    protected function _prepareLayout()
    {
        $total = $this->getLayout()
            ->createBlock('practice_reward/adminhtml_customer_edit_tab_reward_management_balance');

        $this->setChild('balance', $total);

        $update = $this->getLayout()
            ->createBlock('practice_reward/adminhtml_customer_edit_tab_reward_management_update');

        $this->setChild('update', $update);

        return parent::_prepareLayout();
    }
}
