<?php

/**
 * Customer My Account -> Reward Points container
 *
 * @category    Practice
 * @package     Practice_Reward
 */
class Practice_Reward_Block_Customer_Reward extends Mage_Core_Block_Template
{
    /**
     * Set template variables
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setBackUrl($this->getUrl('customer/account/'));
        return parent::_toHtml();
    }
}
