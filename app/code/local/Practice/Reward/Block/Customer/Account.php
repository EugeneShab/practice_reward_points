<?php

/**
 * Customer Account empty block (using only just for adding RP link to tab)
 *
 * @category    Practice
 * @package     Practice_Reward
 */
class Practice_Reward_Block_Customer_Account extends Mage_Core_Block_Abstract
{
    /**
     * Add RP link to tab if we have all rates
     *
     * @return Practice_Reward_Block_Customer_Account
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $navigationBlock = $this->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock && Mage::helper('practice_reward')->isEnabledOnFront()) {
            $navigationBlock->addLink('practice_reward', 'practice_reward/customer/info/',
                Mage::helper('practice_reward')->__('Reward Points'));
        }
        return $this;
    }
}
