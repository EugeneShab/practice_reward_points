<?php

/**
 * Customer account reward history block
 *
 * @category    Practice
 * @package     Practice_Reward
 */
class Practice_Reward_Block_Customer_Reward_History extends Mage_Core_Block_Template
{
    /**
     * History records collection
     *
     * @var Practice_Reward_Model_Mysql4_Reward_History_Collection
     */
    protected $_collection = null;

    /**
     * Get history collection if needed
     *
     * @return Practice_Reward_Model_Mysql4_Reward_History_Collection|false
     */
    public function getHistory()
    {
        if (0 == $this->_getCollection()->getSize()) {
            return false;
        }
        return $this->_collection;
    }

    /**
     * History item points delta getter
     *
     * @param Practice_Reward_Model_Reward_History $item
     * @return string
     */
    public function getPointsDelta(Practice_Reward_Model_Reward_History $item)
    {
        return Mage::helper('practice_reward')->formatPointsDelta($item->getPointsDelta());
    }

    /**
     * History item points balance getter
     *
     * @param Practice_Reward_Model_Reward_History $item
     * @return string
     */
    public function getPointsBalance(Practice_Reward_Model_Reward_History $item)
    {
        return $item->getPointsBalance();
    }

    /**
     * History item currency balance getter
     *
     * @param Practice_Reward_Model_Reward_History $item
     * @return string
     */
    public function getCurrencyBalance(Practice_Reward_Model_Reward_History $item)
    {
        return Mage::helper('core')->currency($item->getCurrencyAmount());
    }

    /**
     * History item reference message getter
     *
     * @param Practice_Reward_Model_Reward_History $item
     * @return string
     */
    public function getMessage(Practice_Reward_Model_Reward_History $item)
    {
        return $item->getMessage();
    }

    /**
     * History item reference additional explanation getter
     *
     * @param Practice_Reward_Model_Reward_History $item
     * @return string
     */
    public function getExplanation(Practice_Reward_Model_Reward_History $item)
    {
        return ''; // TODO
    }

    /**
     * History item creation date getter
     *
     * @param Practice_Reward_Model_Reward_History $item
     * @return string
     */
    public function getDate(Practice_Reward_Model_Reward_History $item)
    {
        return Mage::helper('core')->formatDate($item->getCreatedAt(), 'short', true);
    }

    /**
     * History item expiration date getter
     *
     * @param Practice_Reward_Model_Reward_History $item
     * @return string
     */
    public function getExpirationDate(Practice_Reward_Model_Reward_History $item)
    {
        $expiresAt = $item->getExpiresAt();
        if ($expiresAt) {
            return Mage::helper('core')->formatDate($expiresAt, 'short', true);
        }
        return '';
    }

    /**
     * Return reword points update history collection by customer and website
     *
     * @return Practice_Reward_Model_Mysql4_Reward_History_Collection
     */
    protected function _getCollection()
    {
        if (!$this->_collection) {
            $websiteId = Mage::app()->getWebsite()->getId();
            $this->_collection = Mage::getModel('practice_reward/reward_history')->getCollection()
                ->addCustomerFilter(Mage::getSingleton('customer/session')->getCustomerId())
                ->addWebsiteFilter($websiteId)
                ->setExpiryConfig(Mage::helper('practice_reward')->getExpiryConfig())
                ->addExpirationDate($websiteId)
                ->skipExpiredDuplicates()
                ->setDefaultOrder()
            ;
        }
        return $this->_collection;
    }

    /**
     * Instantiate Pagination
     *
     * @return Practice_Reward_Block_Customer_Reward_History
     */
    protected function _prepareLayout()
    {
        if ($this->_isEnabled()) {
            $pager = $this->getLayout()->createBlock('page/html_pager', 'reward.history.pager')
                ->setCollection($this->_getCollection())->setIsOutputRequired(false)
            ;
            $this->setChild('pager', $pager);
        }
        return parent::_prepareLayout();
    }

    /**
     * Whether the history may show up
     *
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->_isEnabled()) {
            return parent::_toHtml();
        }
        return '';
    }

    /**
     * Whether the history is supposed to be rendered
     *
     * @return bool
     */
    protected function _isEnabled()
    {
        return Mage::helper('practice_reward')->isEnabledOnFront()
            && Mage::helper('practice_reward')->getGeneralConfig('publish_history');
    }
}
