<?php

/**
 * Checkout Tooltip block to show checkout cart message for gaining reward points
 *
 * @category    Practice
 * @package     Practice_Reward
 */
class Practice_Reward_Block_Tooltip_Checkout extends Practice_Reward_Block_Tooltip
{
    /**
     * Set quote to the reward action instance
     *
     * @param int|string $action
     */
    public function initRewardType($action)
    {
        parent::initRewardType($action);
        if ($this->_actionInstance) {
            $this->_actionInstance->setQuote(Mage::getSingleton('checkout/session')->getQuote());
        }
    }
}
