<?php

/**
 * Reward Helper for operations with customer
 *
 * @category    Practice
 * @package     Practice_Reward
 */
class Practice_Reward_Helper_Customer extends Mage_Core_Helper_Abstract
{
    /**
     * Return Unsubscribe notification URL
     *
     * @param string|boolean $notification Notification type
     * @param int|string|Mage_Core_Model_Store $storeId
     * @return string
     */
    public function getUnsubscribeUrl($notification = false, $storeId = null)
    {
        $params = array();

        if ($notification) {
            $params['notification'] = $notification;
        }
        if (!is_null($storeId)) {
            $params['store_id'] = $storeId;
        }
        return Mage::app()->getStore($storeId)->getUrl('practice_reward/customer/unsubscribe/', $params);
    }
}
