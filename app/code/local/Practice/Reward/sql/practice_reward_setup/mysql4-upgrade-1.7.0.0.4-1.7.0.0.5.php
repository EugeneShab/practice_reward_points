<?php
/**
 *
 * @category    Practice
 * @package     Practice_Reward
 */

/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('order', 'reward_points_balance_to_refund', array('type' => 'int'));
$installer->addAttribute('creditmemo', 'reward_points_balance_to_refund', array('type' => 'int'));

$installer->endSetup();
