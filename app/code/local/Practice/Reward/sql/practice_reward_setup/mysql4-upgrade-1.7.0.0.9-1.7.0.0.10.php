<?php
/**
 *
 * @category    Practice
 * @package     Practice_Reward
 */

/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;
$installer->startSetup();
$installer->getConnection()->changeColumn($installer->getTable('practice_reward/reward_rate'), 'customer_group_id',
    'customer_group_id', "SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0'");
$installer->endSetup();
