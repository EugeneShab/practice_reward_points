<?php
/**
 *
 * @category    Practice
 * @package     Practice_Reward
 */

/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;
$installer->startSetup();

$installer->run("
CREATE TABLE IF NOT EXISTS `{$installer->getTable('practice_reward/reward_salesrule')}` (
    `rule_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `points_delta` int(11) UNSIGNED NOT NULL DEFAULT '0',
    KEY `FK_REWARD_SALESRULE_RULE_ID` (`rule_id`),
    CONSTRAINT `FK_REWARD_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `{$installer->getTable('salesrule/rule')}` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8
");

$installer->addAttribute('order', 'reward_salesrule_points', array('type' => 'int'));

$installer->endSetup();
