<?php
/**
 *
 * @category    Practice
 * @package     Practice_Reward
 */

/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('order', 'reward_points_balance_refunded', array('type' => 'int'));

$installer->addAttribute('invoice', 'reward_points_balance', array('type' => 'int'));
$installer->addAttribute('invoice', 'base_reward_currency_amount', array('type' => 'decimal'));
$installer->addAttribute('invoice', 'reward_currency_amount', array('type' => 'decimal'));

$installer->addAttribute('creditmemo', 'reward_points_balance', array('type' => 'int'));
$installer->addAttribute('creditmemo', 'base_reward_currency_amount', array('type' => 'decimal'));
$installer->addAttribute('creditmemo', 'reward_currency_amount', array('type' => 'decimal'));

$installer->endSetup();
