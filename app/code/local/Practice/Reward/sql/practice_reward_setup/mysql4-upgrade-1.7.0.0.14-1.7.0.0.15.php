<?php
/**
 *
 * @category    Practice
 * @package     Practice_Reward
 */


/* @var $installer Practice_Reward_Model_Mysql4_Setup */
$installer = $this;

$attributes = array(
    'reward_update_notification',
    'reward_warning_notification'
);
$entityTypeCode = 'customer';

foreach ($attributes as $attributeCode) {
    $attributeId = $installer->getAttributeId($entityTypeCode, $attributeCode);
    $installer->updateAttribute($entityTypeCode, $attributeId, 'is_user_defined', 0);
    $installer->updateAttribute($entityTypeCode, $attributeId, 'is_system', 1);
    $installer->updateAttribute($entityTypeCode, $attributeId, 'is_hidden', 1);
}
