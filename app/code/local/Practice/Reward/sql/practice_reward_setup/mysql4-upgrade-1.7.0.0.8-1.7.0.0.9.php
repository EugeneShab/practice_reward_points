<?php
/**
 *
 * @category    Practice
 * @package     Practice_Reward
 */

/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;
$installer->startSetup();
$installer->getConnection()->addColumn($this->getTable('practice_reward_history'), 'is_duplicate_of', "int(11) unsigned DEFAULT NULL AFTER `is_expired`");
$installer->endSetup();
