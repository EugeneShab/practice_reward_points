<?php
/**
 *
 * @category    Practice
 * @package     Practice_Reward
 */

/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;
$installer->startSetup();
$installer->getConnection()->changeColumn($this->getTable('practice_reward_history'), 'expired_at', 'expired_at_static', "datetime NULL DEFAULT '0000-00-00 00:00:00'");
$installer->getConnection()->addColumn($this->getTable('practice_reward_history'), 'expired_at_dynamic', "datetime NULL DEFAULT '0000-00-00 00:00:00' AFTER `expired_at_static`");
$installer->endSetup();
