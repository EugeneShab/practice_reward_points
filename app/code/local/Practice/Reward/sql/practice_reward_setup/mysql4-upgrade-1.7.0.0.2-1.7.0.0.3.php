<?php
/**
 *
 * @category    Practice
 * @package     Practice_Reward
 */

/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;
$installer->startSetup();
$installer->getConnection()->addColumn($this->getTable('practice_reward_history'), 'points_used', "int(11) NOT NULL DEFAULT '0' AFTER `points_delta`");
$installer->getConnection()->addColumn($this->getTable('practice_reward_history'), 'is_expired', "tinyint(3) NOT NULL DEFAULT '0' AFTER `expired_at`");
$installer->endSetup();
