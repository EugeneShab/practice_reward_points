<?php
/**
 * @category    Practice
 * @package     Practice_Reward
 */

/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('quote', 'use_reward_points', array('type' => 'int'));
$installer->addAttribute('quote', 'reward_points_balance', array('type' => 'int'));
$installer->addAttribute('quote', 'base_reward_currency_amount', array('type' => 'decimal'));
$installer->addAttribute('quote', 'reward_currency_amount', array('type' => 'decimal'));

$installer->addAttribute('quote_address', 'reward_points_balance', array('type' => 'int'));
$installer->addAttribute('quote_address', 'base_reward_currency_amount', array('type' => 'decimal'));
$installer->addAttribute('quote_address', 'reward_currency_amount', array('type' => 'decimal'));

$installer->addAttribute('order', 'reward_points_balance', array('type' => 'int'));
$installer->addAttribute('order', 'base_reward_currency_amount', array('type' => 'decimal'));
$installer->addAttribute('order', 'reward_currency_amount', array('type' => 'decimal'));
$installer->addAttribute('order', 'base_reward_currency_amount_invoiced', array('type' => 'decimal'));
$installer->addAttribute('order', 'reward_currency_amount_invoiced', array('type' => 'decimal'));
$installer->addAttribute('order', 'base_reward_currency_amount_refunded', array('type' => 'decimal'));
$installer->addAttribute('order', 'reward_currency_amount_refunded', array('type' => 'decimal'));

$installer->addAttribute('invoice', 'base_reward_currency_amount', array('type' => 'decimal'));
$installer->addAttribute('invoice', 'reward_currency_amount', array('type' => 'decimal'));

$installer->addAttribute('creditmemo', 'base_reward_currency_amount', array('type' => 'decimal'));
$installer->addAttribute('creditmemo', 'reward_currency_amount', array('type' => 'decimal'));

$installer->endSetup();
