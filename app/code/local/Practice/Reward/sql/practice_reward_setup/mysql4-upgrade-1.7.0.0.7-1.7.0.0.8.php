<?php
/**
 *
 * @category    Practice
 * @package     Practice_Reward
 */

/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;
$installer->startSetup();
$installer->getConnection()->dropForeignKey($installer->getTable('practice_reward/reward'), 'FK_REWARD_WEBSITE_ID');
$installer->getConnection()->changeColumn($installer->getTable('practice_reward/reward'), 'website_id',
    'website_id', 'SMALLINT(5) UNSIGNED DEFAULT NULL');
$installer->getConnection()->addColumn($installer->getTable('practice_reward/reward'),
    'website_currency_code', 'CHAR(3) DEFAULT NULL AFTER `points_balance`');
$installer->getConnection()->dropForeignKey($installer->getTable('practice_reward/reward_history'), 'FK_REWARD_HISTORY_STORE_ID');
$installer->getConnection()->changeColumn($installer->getTable('practice_reward/reward_history'), 'store_id',
    'store_id', 'SMALLINT(5) UNSIGNED DEFAULT NULL');
$installer->getConnection()->addConstraint('FK_REWARD_HISTORY_WEBSITE_ID', $installer->getTable('practice_reward/reward_history'),
    'website_id', $installer->getTable('core/website'), 'website_id');
$installer->getConnection()->addConstraint('FK_REWARD_HISTORY_STORE_ID', $installer->getTable('practice_reward/reward_history'),
    'store_id', $installer->getTable('core/store'), 'store_id', 'SET NULL', 'CASCADE');
$installer->endSetup();
