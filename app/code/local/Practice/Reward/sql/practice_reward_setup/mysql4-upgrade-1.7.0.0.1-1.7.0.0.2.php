<?php

/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('order', 'reward_points_balance_refunded', array('type' => 'int'));

$installer->removeAttribute('invoice', 'base_reward_currency_amount');
$installer->removeAttribute('invoice', 'reward_currency_amount');

$installer->removeAttribute('creditmemo', 'base_reward_currency_amount');
$installer->removeAttribute('creditmemo', 'reward_currency_amount');

$installer->endSetup();
