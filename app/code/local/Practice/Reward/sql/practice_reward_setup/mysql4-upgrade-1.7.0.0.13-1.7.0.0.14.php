<?php
/**
 *
 * @category    Practice
 * @package     Practice_Reward
 */

/* @var $installer Practice_Reward_Model_Mysql4_Setup */
$installer = $this;
$installer->startSetup();

$columnsToMove = array(
    'reward_update_notification',
    'reward_warning_notification'
);

foreach ($columnsToMove as $column) {
    $installer->addAttribute('customer', $column,
        array('type' => 'int', 'visible' => 0, 'visible_on_front' => 1)
    );
    $installer->getConnection()->dropColumn(
        $installer->getTable('practice_reward'), $column
    );
}

$installer->endSetup();
