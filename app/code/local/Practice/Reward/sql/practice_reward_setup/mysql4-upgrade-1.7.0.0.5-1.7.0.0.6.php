<?php
/**
 *
 * @category    Practice
 * @package     Practice_Reward
 */

/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('quote', 'base_reward_currency_amount', array('type' => 'decimal'));
$installer->addAttribute('quote', 'reward_currency_amount', array('type' => 'decimal'));

$installer->endSetup();
